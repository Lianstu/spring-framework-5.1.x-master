package com.lianxiaoshuai.shejimoshi.danli;

import org.aopalliance.aop.Advice;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.Proxy;
import java.util.Hashtable;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 饿汉模式
 */
public class SingleHungry {

	private  static SingleHungry singleHungry = new SingleHungry();
	private SingleHungry() {
		System.out.println("构造函数");
	}

	private static SingleHungry getInstance() {
		return singleHungry;
	}

	public static void main(String[] args) {
		ReentrantLock reentrantLock;
		Hashtable hashtable;
		Advice advice;
		ProxyFactory factory;
	}
}
