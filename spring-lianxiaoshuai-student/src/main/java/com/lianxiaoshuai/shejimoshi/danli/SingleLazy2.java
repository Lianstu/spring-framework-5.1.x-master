package com.lianxiaoshuai.shejimoshi.danli;

/**
 * 懒汉模式内部静态类模式
 */
public class SingleLazy2 {

	private SingleLazy2() {
		System.out.println("构造函数");
	}

	private static class SingleHolder {
		private static SingleLazy2 singleLazy2 = new SingleLazy2();
	}

	private static SingleLazy2 getInstance() {
		return SingleHolder.singleLazy2;
	}

}
