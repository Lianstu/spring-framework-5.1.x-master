package com.lianxiaoshuai.shejimoshi.danli;

/**
 * 懒汉模式双重校验
 */
public class SingleLazy1 {

	private static SingleLazy1 singleLazy1;

	private SingleLazy1 () {
		System.out.println("构造函数");
	}

	private static SingleLazy1 getSingleLazy1() {
		if (singleLazy1 == null) {
			synchronized (SingleLazy1.class) {
				if (singleLazy1 == null) {
					singleLazy1 = new SingleLazy1();
				}
			}
		}
		return singleLazy1;
	}
}
