package com.lianxiaoshuai.test_mvc43.test;

import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;

public class Test {
	public static void main(String[] args) {

		/**
		 * SpringServletContainerInitializer可以被servlet 3.0以上的容器自动启动
		 *
		 * @HandlesTypes ?
		 *
		 * 要看链接：https://www.cnblogs.com/davidwang456/p/6491067.html
		 */
		SpringServletContainerInitializer springServletContainerInitializer;

		/**
		 * servlet 3.0 ServletContainerInitializer 使用spring的WebApplicationInitializer来支持对servlet container的基于编程的配置支持
		 */
		WebApplicationInitializer webApplicationInitializer;


		/**
		 * org.springframework.web.servlet.DispatcherServlet ==> 重要
		 *
		 * servlet ==> 小程序
		 *
		 * 1. org.springframework.web.servlet.DispatcherServlet 这个方法很重要
		 * 		有个 onRefresh 父类FrameworkServlet没有处理 由DispatcherServlet处理
		 * 			initStrategies ==> 初始化策略
		 *
		 * 	Detect：发现
		 *	AbstractDetectingUrlHandlerMapping#initApplicationContext()#detectHandlers() 会处理handlerMap
		 *		调用这个类 AbstractUrlHandlerMapping 的方法 	会 把 handlerMap 给封装起来
		 *
		 *		AbstractHandlerMethodMapping 继承 AbstractHandlerMapping
		 *		AbstractUrlHandlerMapping  继承 AbstractHandlerMapping
		 *
		 * MultipartResolver 文件上传相关内容
		 *
		 * HandlerMapping
		 * 		1. 两个默认的加载：
		 * 			org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping,\
		 *			org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping
		 * 		2. 注解方式的代码是这个类实现的 DefaultAnnotationHandlerMapping
		 *
		 * HandlerAdapter
		 *
		 *
		 * 2. controller的几种写法
		 *
		 * HandlerExecutionChain
		 *
		 * HandlerInterceptor 拦截器
		 *
		 * HttpMessageConverter : 定义返回内容是视图、还是json 等内容
		 *
		 *
		 * 3. 如何读取参数： 反射是拿不到参数名的 ==> 是使用 asm 框架读取字节码文件
		 * 		为什么使用 asm、如果只有一个参数只需要method.invoke(bean, param)就可以了,如果参数是多个就不知掉顺序了
		 *
		 *
		 * 4. 拦截器顺序  实现 WebMvcConfigurer 、重写 addInterceptors 这个方法、使多个拦截器有序
		 *
		 */

		HandlerInterceptorAdapter handlerInterceptorAdapter;

		HandlerMethodReturnValueHandler handlerMethodReturnValueHandler;  // restful api
		RequestResponseBodyMethodProcessor requestResponseBodyMethodProcessor;


		ModelAndViewContainer modelAndViewContainer;

	}
}
