package com.lianxiaoshuai.testnewbean01_06;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		/**
		 * 1. DBConfig 实现 ImportAware 能够得到上下文的注解、
		 * redis的 的demo @Import({RedissonHttpSessionConfiguration.class}) 、通过这个给被注解的内容赋值
		 *
		 * https://blog.csdn.net/zgyjk/article/details/80780165
 		 */

		/**
		 * JDK动态代理给Spring事务的坑  https://blog.csdn.net/bntx2jsqfehy7/article/details/79040349
		 *
		 * 2. 解决问题根据用户不同使用不同的dao
		 *
		 * ==> 实现 implements ApplicationContextAware
		 *
		 * ==> @Autowired 加在map上面
		 */

		/**
		 * AopConfig 加了 @EnableAspectJAutoProxy 加个后置处理器把对象变成代理对象
		 *
		 * AbstractAutoProxyCreator : aop 就是通过他实现的 ? 实现一个 BeanPostProcessor 返回个代理对象
		 *
		 * PostProcessorRegistrationDelegate#invokeBeanFactoryPostProcessors
		 *
		 * 3 .@EnableAspectJAutoProxy ===> aop 代理的原理
		 *
		 *
		 */

		/**
		 * AbstractAutowireCapableBeanFactory.
		 *
		 *
		 *
		 * FactoryMethod ？
		 */

		AnnotationConfigApplicationContext ct = new AnnotationConfigApplicationContext();
	}
}
