package com.lianxiaoshuai.testnewbean01_06.factorymethod;


import org.springframework.context.annotation.Bean;

public class factoryMethodTest {

	/**
	 * 这个情况是会被代理的：==getServiceA== 只会打印一次
	 *
	 *
	 * 如果 private ServiceA getServiceA() 加static、就会是两边、没有办法被代理
	 *
	 * ==> 源码部分 ConfigurationClassBeanDefinitionReader # loadBeanDefinitionsForBeanMethod # 228 行
	 *
	 */
	@Bean
	private ServiceA getServiceA() {
		System.out.println("==getServiceA==");
		return new ServiceA();
	}


	@Bean
	private ServiceB getServiceB() {
		getServiceA();
		return new ServiceB();
	}

}
