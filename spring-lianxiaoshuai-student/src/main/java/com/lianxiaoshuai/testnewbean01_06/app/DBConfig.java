package com.lianxiaoshuai.testnewbean01_06.app;

import org.springframework.context.annotation.ImportAware;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.stereotype.Component;

@Component
public class DBConfig implements ImportAware{

	@Override
	public void setImportMetadata(AnnotationMetadata importMetadata) {
		importMetadata.getMetaAnnotationTypes("annotationName");
	}
	// @Component 、@Configuration 都会加到Spring 容器中
}
