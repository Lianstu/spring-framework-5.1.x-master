package com.lianxiaoshuai.testnewbean01_06.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by lianshaoshuai on 2020/6/27.
 */
public class UserServiceImpl implements ApplicationContextAware {

	ApplicationContext applicationContext;


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext; // 这个东西就能到上下文内容
	}
}
