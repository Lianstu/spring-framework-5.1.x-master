package com.lianxiaoshuai.testym12_01;

import com.lianxiaoshuai.testym12_01.dao.UserDao;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Test22 {

    public static void main(String[] args) {
        // 1. beanFactory 是一个Spring 提供的工厂类，但是他不是bean
        BeanFactory beanFactory = new ClassPathXmlApplicationContext();

        // 2.1 factoryBean 是个spring 管理的bean、可以返回一个其他的类
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(UserDao.class);
		UserDao tempFactoryBean = (UserDao) annotationConfigApplicationContext.getBean(UserDao.class);
        // 2.2 如果要取 daoFactoryBean 本身就需要加上一个&
        DaoFactoryBean daoFactoryBean = (DaoFactoryBean) annotationConfigApplicationContext.getBean("&daoFactoryBean");

        //2.3 典型的应用: @MapperScan 
        // mybatis 的getObject 先afterPropertiesSet 后处理了sqlSessionFactory
        // SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();

        // @PostConstruct ==> 可以做缓存的预热
		AutowiredAnnotationBeanPostProcessor autowiredAnnotationBeanPostProcessor;

        // 3. 初始化Spring环境的方法
        // xml ClassPathXmlApplicationContext  类的扫描  单独bean的注册 声明和注册
        // javaConfig AnnotationConfigApplicationContext  类的扫描    类的定义  如果只有@Service、没有@scan 也初始化不了
        // 为什么初始化Spring环境: 把交给Spring容器的类 实例化
        annotationConfigApplicationContext.scan(""); // 是做了一个实例化new Instance()
        // annotationConfigApplicationContext.register(ShopServerApplication.class); // 注册一个类
        annotationConfigApplicationContext.refresh();

        // @Profile() 先去实例化、设置环境、后扫描


		BeanFactory beanFactory1;
		ApplicationContext applicationContext;

		AnnotationAwareAspectJAutoProxyCreator annotationAwareAspectJAutoProxyCreator;



		MethodInterceptor methodInterceptor;

    }
}
