package com.lianxiaoshuai.testym12_01.test;

import com.lianxiaoshuai.testym12_01.app.AppConfig;
import com.lianxiaoshuai.testym12_01.dao.UserDao;
import groovy.util.AbstractFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.LifecycleProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;



public class Test23 {
    public static void main(String[] args) {
        // 0. 普通shying
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        UserDao userDao = (UserDao) annotationConfigApplicationContext.getBean(UserDao.class);
        userDao.query();




        // 1. 如果 AppConfig 不加 @ComponentScan("test")、 需要register、然后手动refresh、否则会报错
        AnnotationConfigApplicationContext annotationConfigApplicationContext1 = new AnnotationConfigApplicationContext();
        annotationConfigApplicationContext1.register(UserDao.class); //  这个做的事情是读取放到beanDefine 里面
        annotationConfigApplicationContext1.refresh(); // 做了初始化
        UserDao userDao1 = (UserDao) annotationConfigApplicationContext1.getBean(UserDao.class);
        userDao1.query();

        // 2. 笔记：
        // AnnotationConfigApplicationContext、
        // AnnotatedBeanDefinitionReader 、
        // DefaultListableBeanFactory.registerBeanDefinition ==> 这个工厂类很重要

        // AbstractApplicationContext.refresh (516) ==> 做实例化

        // BeanPostProcessor   bean后置处理器   扩展点1   控制处理器执行顺序 implements PriorityOrdered   的 getOrder 方法 值小的在前面 (重点看啦)
            // AbstractAutoProxyCreator : aop 就是通过他实现的 ? 实现一个 BeanPostProcessor 返回个代理对象
		    // AutowiredAnnotationBeanPostProcessor : @Autowired 就是通过这个搞进来的
				//==> 先byType如果找到多个就通过byName找、自动装配技术byType、byName、自动装配类型：四种、无、构造函数、byType、byName
        // BeanFactoryPostProcessor   bean工厂的后置处理器   扩展点2

		AbstractBeanDefinition abstractBeanDefinition;
		BeanDefinitionRegistryPostProcessor beanDefinitionRegistryPostProcessor;


		LifecycleProcessor lifecycleProcessor;


    }
}
