package com.lianxiaoshuai.testym12_01.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("testym12")
public class AppConfig {
}
