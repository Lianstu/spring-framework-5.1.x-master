package com.lianxiaoshuai.testym12_01;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

/**
 *   getObject  返回的其实是 TempFactoryBean 这个东西
 *
 *   容器中存在两个对向
 *   1. 在Spring中存的beanDefine 是 daoFactoryBean ==> TempFactoryBean
 *   2. DaoFactoryBean对象是用 "&DaoFactoryBean" 存起来
 *
 *   bean: 在Spring容器中、受Spring管理: 举例
 */
@Component
public class DaoFactoryBean implements FactoryBean {

    public void testBean() {
        System.out.println("DaoFactoryBean");
    }
    @Override
    public Object getObject() throws Exception {
        return new TempFactoryBean();
    }

    @Override
    public Class<?> getObjectType() {
        return TempFactoryBean.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
