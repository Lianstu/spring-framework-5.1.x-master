package com.lianxiaoshuai.testnewbean02_07.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author gaoh
 * @version 1.0
 * @date 2020/5/8 9:55
 */
@ComponentScan("com.lianxiaoshuai.testnewbean02_07")
@Configuration
public class Appconfig {

}
