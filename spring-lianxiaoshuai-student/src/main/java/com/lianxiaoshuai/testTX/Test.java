package com.lianxiaoshuai.testTX;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.TransactionManagementConfigurationSelector;
import org.springframework.transaction.support.*;

import java.sql.DriverManager;

public class Test {
	public static void main(String[] args) {
		/**
		 * https://www.cnblogs.com/dennyzhangdd/p/9602673.html
		 *
		 *
		 * 注解方式用的是动态代理、
		 */
		TransactionTemplate transactionTemplate;
		TransactionCallback transactionCallback;
		TransactionCallbackWithoutResult transactionCallbackWithoutResult;
		AbstractPlatformTransactionManager abstractPlatformTransactionManager;
		DataSourceTransactionManager dataSourceTransactionManager;
		// DataSourceTransactionmanager dataSourceTransactionmanager;

		// JdkDynamicAutoProxyConfiguration jdkDynamicAutoProxyConfiguration;
//		TransactionAutoConfiguration transactionAutoConfiguration;

		TransactionManagementConfigurationSelector transactionManagementConfigurationSelector;
		/**
		 * 注解方式用的是动态代理：
		 *
		 * 1. JdkDynamicAutoProxyConfiguration
		 * @EnableTransactionManagement(proxyTargetClass = false)，即proxyTargetClass = false表示是JDK动态代理支持的是：面向接口代理。
		 * @ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "false", matchIfMissing = false)，即spring.aop.proxy-target-class=false时生效，且没有这个配置不生效。
		 *
		 * 2. CglibAutoProxyConfiguration：
		 * @EnableTransactionManagement(proxyTargetClass = true)，即proxyTargetClass = true标识Cglib代理支持的是子类继承代理。
		 * @ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "true", matchIfMissing = true)，即spring.aop.proxy-target-class=true时生效，且没有这个配置默认生效。
		 */
		/**
		 * mybatis 会 复写这个类的
		 */
		TransactionSynchronization transactionSynchronization;
		// DefaultSqlSession defaultSqlSession;

//		SqlSessionUtils sqlSessionUtils;
//
//		SqlSessionSynchronization sqlSessionSynchronization;


		DriverManager driverManager;
	}
}
