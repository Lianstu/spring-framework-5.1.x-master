package com.lianxiaoshuai.test_boot44.test;


public class Test {
	/**
	 * EventPublishLister#
	 * 这个类很重要:
	 * 1.首先他会广播一个事件
	 * 		对应代码： for(final ApplicationListener : getApplicationListeners(event, type))
	 * 		getApplicationListeners 这两个参数就是事件类型、告诉所有的监听器现有一个 type 的 event、你们都感兴趣吗
	 *
	 * 2 告诉监听器：
	 * 	getApplicationListeners 告诉所有监听器、循环遍历所有的监听器
	 * 	监听器接收到事件、判断自己是否需要处理
	 *
	 * 3. 监听器怎么知道自己是否需要处理？
	 * 	第一步：
	 * 		supportEventType(event)、smartListener.supportEventType   会返回一个bool、如果感兴趣加到一个list里面
	 *	第二步：
	 *		在监听器回调的时候、还可以进行对事件类型的判断、如果都不感兴趣就不执行了
	 *
	 * 4. 获得所有对这个事件感兴趣的监听器、遍历其 onApplicationEvent 方法、这里传入一个 ApplicationStartingEvent 事件过去
	 *
	 * 		在spring中定义了11个监听器
	 *
	 * 5. initialMulticaster 可以看到是 SimpleApplicationEventMulticaster类型的对象
	 * 	一个广播事件、一个是执行 listener的 onApplicationEvent 方法
	 *
	 *
	 *
	 *
	 *
	 *
	 *	源码部分看： spring-boot那个项目
	 *
	 * 	笔记：这个博客写的还可以啦 ==> https://www.cnblogs.com/ymbj/tag/SpringBoot源码专题/
	 */
}
