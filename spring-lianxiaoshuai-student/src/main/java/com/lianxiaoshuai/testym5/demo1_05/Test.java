package com.lianxiaoshuai.testym5.demo1_05;


import com.lianxiaoshuai.testnewbean02_07.config.Appconfig;
import com.lianxiaoshuai.testym5.demo1_05.dao.UserDaoImp2;
import org.springframework.cglib.core.SpringNamingPolicy;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		// cglib 代理的用法

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Appconfig.class);

		/**
		 * 如何用cglib、已经Spring中cglic如何完成代理的
		 */
		Enhancer enhancer = new Enhancer();
		enhancer.setSuperclass(UserDaoImp2.class); // 设置supper
		enhancer.setNamingPolicy(SpringNamingPolicy.INSTANCE);
		enhancer.setCallback(new TestMethodCallback());

		UserDaoImp2 userDaoImp2 = (UserDaoImp2) enhancer.create();
		userDaoImp2.query();


		/**
		 * spring 部分看 幕布笔记 https://mubu.com/doc/5Ov1CUSQ7nL
		 */
	}
}
