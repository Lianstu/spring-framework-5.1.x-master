package com.lianxiaoshuai.testym5.demo1_05.app;

import com.lianxiaoshuai.testym5.demo1_05.dao.UserDaoImp1;
import com.lianxiaoshuai.testym5.demo1_05.dao.UserDaoImp2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.lianxiaoshuai.testym5.demo1_05"})


public class Appconfig {

	@Bean
	public UserDaoImp1 userDaoImp1(){

		return new UserDaoImp1();
	}


	@Bean
	public UserDaoImp2 userDaoImp2(){

		return new UserDaoImp2();
	}

}
