package com.lianxiaoshuai.testym5.demo1_05;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;


public class TestMethodCallback implements MethodInterceptor{

	/**
	 * 方法拦截器
	 * @param o
	 * @param method
	 * @param objects
	 * @param methodProxy
	 * @return
	 * @throws Throwable
	 */
	@Override
	public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
		System.out.println("<====TestMethodCallback==>");

		return methodProxy.invokeSuper(o, objects);
	}
}
