package com.lianxiaoshuai.testym4_04.demo1.app;

import com.lianxiaoshuai.testym4_04.demo1.dao.UserDao1;
import com.lianxiaoshuai.testym4_04.demo1.imports.MyImportSelector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@ComponentScan({"com.lianxiaoshuai.testym4_04.demo1"})
@Import(MyImportSelector.class) // 也可以用 EanbleLuabn 替代


public class Appconfig {

	@Bean
	public UserDao1 userDao1(){

		return new UserDao1();
	}

}
