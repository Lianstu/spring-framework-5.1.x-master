package com.lianxiaoshuai.testym4_04.demo1.imports;

import com.lianxiaoshuai.testym4_04.demo1.dao.UserDao3;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
/**
 * 不需要引入到容器中
 */
public class MyImportSelector implements ImportSelector{

	@Override
	public String[] selectImports(AnnotationMetadata importingClassMetadata) {

		return new String[] {UserDao3.class.getName()};
	}
}
