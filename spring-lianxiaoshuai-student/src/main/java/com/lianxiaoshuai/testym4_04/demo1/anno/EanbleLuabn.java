package com.lianxiaoshuai.testym4_04.demo1.anno;

import com.lianxiaoshuai.testym4_04.demo2.imports.MyImportSelector;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Import(MyImportSelector.class)
public @interface EanbleLuabn {

}
