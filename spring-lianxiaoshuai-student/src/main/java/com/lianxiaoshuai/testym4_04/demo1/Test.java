package com.lianxiaoshuai.testym4_04.demo1;


import com.lianxiaoshuai.testnewbean02_07.config.Appconfig;
import com.lianxiaoshuai.testym4_04.demo1.dao.UserDao3;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		// mapperScan 的原理
		// MapperScanRegister 可以在实例化bean 设置进去一个

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Appconfig.class);
		UserDao3 userDao3 = ctx.getBean(UserDao3.class);
		userDao3.query();
	}
}
