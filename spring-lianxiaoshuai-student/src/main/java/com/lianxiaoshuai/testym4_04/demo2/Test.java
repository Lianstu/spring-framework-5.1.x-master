package com.lianxiaoshuai.testym4_04.demo2;


import com.lianxiaoshuai.testym4_04.demo2.app.Appconfig;
import com.lianxiaoshuai.testym4_04.demo2.dao.UserDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		// EanbleLuabn 的类似spring原理

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Appconfig.class);
		UserDao userDao = (UserDao) ctx.getBean("userDao1");
		userDao.query();
	}
}
