package com.lianxiaoshuai.testym4_04.demo2.imports;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


public class MyInvocationHandler implements InvocationHandler{
	private Object target;

	public MyInvocationHandler (Object target) {
		this.target = target;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("==> MyInvocationHandler");
		return method.invoke(target, args);
	}
}
