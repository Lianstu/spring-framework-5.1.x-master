package com.lianxiaoshuai.testym4_04.demo2.app;

import com.lianxiaoshuai.testym4_04.demo2.anno.EanbleLuabn;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.lianxiaoshuai.testym4_04.demo2"})
@EanbleLuabn
public class Appconfig {

}
