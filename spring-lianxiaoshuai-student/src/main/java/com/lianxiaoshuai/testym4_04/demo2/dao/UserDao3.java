package com.lianxiaoshuai.testym4_04.demo2.dao;

import com.lianxiaoshuai.testym4_04.demo2.imports.MyInvocationHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Proxy;

public class UserDao3 implements BeanPostProcessor{

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		if (beanName.equals("userDao1")) {
			bean = Proxy.newProxyInstance(this.getClass().getClassLoader(),
					new Class[]{UserDao.class}, new MyInvocationHandler(bean));
		}

		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		// 这个地方要返回null
		return null;
	}

	public void query() {
		System.out.println("===> UserDao3 ");
	}
}
