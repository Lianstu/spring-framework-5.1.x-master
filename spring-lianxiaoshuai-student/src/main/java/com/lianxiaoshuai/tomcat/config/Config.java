package com.lianxiaoshuai.tomcat.config;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: GH
 * @Date: 2019/12/10 23:41
 * @Version 1.0
 */
@Configurable
@ComponentScan("com.lianxiaoshuai")
public class Config {
}
