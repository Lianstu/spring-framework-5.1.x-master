package com.lianxiaoshuai.tomcat.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * @Author: GH
 * @Date: 2019/12/10 23:50
 * @Version 1.0
 */
public class MyWebApplicationInitializer implements WebApplicationInitializer {

	ServletContainerInitializer servletContainerInitializer;

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
		ac.register(Config.class);
		ac.refresh();

		// Create and register the DispatcherServlet
	/*	DispatcherServlet servlet = new DispatcherServlet(ac);
		ServletRegistration.Dynamic registration = servletContext.addServlet("app", servlet);
		registration.setLoadOnStartup(1);
		registration.addMapping("/");*/
	}
}
