package com.lianxiaoshuai.testym3_03;


import com.lianxiaoshuai.testym3_03.app.AppConfig;
import com.lianxiaoshuai.testym3_03.dao.MapperScanCarDao;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
	public static void main(String[] args) {
		// mapperScan 的原理
		// MapperScanRegister 可以在实例化bean 设置进去一个

		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(AppConfig.class);
		ctx.refresh();
		ctx.getBean(MapperScanCarDao.class);
		MapperScanCarDao mapperScanCarDao = (MapperScanCarDao) ctx.getBean("com.lianxiaoshuai.testym3_03.dao.MapperScanCarDao");
	}
}
