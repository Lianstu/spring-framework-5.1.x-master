package com.lianxiaoshuai.testym3_03.app;

import com.lianxiaoshuai.testym3_03.dao.MapperScanCarDao;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

/**
 * Created by lianshaoshuai on 2020/6/24.
 *
 * 在bean实例化之前执行把对应的bean放进去
 *
 * MapperScanCarDao 是个接口、不能被实例化的、借助 MyFactoryBean
 *
 */
public class MapperScanRegister implements ImportBeanDefinitionRegistrar  {

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(MapperScanCarDao.class);
		GenericBeanDefinition beanDefinition = (GenericBeanDefinition) builder.getBeanDefinition();

		beanDefinition.getConstructorArgumentValues().addGenericArgumentValue("com.lianxiaoshuai.testym3.dao.MapperScanCarDao");
		beanDefinition.setBeanClass(MyFactoryBean.class);

		registry.registerBeanDefinition("com.lianxiaoshuai.testym3.dao.MapperScanCarDao", beanDefinition);
	}
}
