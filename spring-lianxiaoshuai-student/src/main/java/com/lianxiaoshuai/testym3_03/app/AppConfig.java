package com.lianxiaoshuai.testym3_03.app;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.lianxiaoshuai.testym3_03")
public class AppConfig {
}
