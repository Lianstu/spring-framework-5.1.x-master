package com.lianxiaoshuai.testym3_03.app;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.lang.Nullable;

import java.lang.reflect.Proxy;

/**
 * @Component
 * 这个东西不能跟 Component 会报错的
 */
public class MyFactoryBean implements FactoryBean{
	private Class clazz;

	public MyFactoryBean (Class clazz) {
		this.clazz = clazz;
	}

	@Nullable
	@Override
	public Object getObject() throws Exception {
		Class[] clazz = new Class[] {this.clazz};

		Object proxy = Proxy.newProxyInstance(this.getClass().getClassLoader(), clazz, new MyInvocationHandler());

		return proxy;
	}

	@Nullable
	@Override
	public Class<?> getObjectType() {
		return clazz;
	}
}
