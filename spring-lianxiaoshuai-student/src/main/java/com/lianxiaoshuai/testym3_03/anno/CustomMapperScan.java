package com.lianxiaoshuai.testym3_03.anno;

/**
 * 自定义一个类似 @MapperScan 的注解
 *
 *
 * MapperScanRegister 继承 ImportBeanDefinitionRegistrar
 */
// @Retention(RetentionPolicy.RUNTIME)
//@Import(MapperScanRegister.class)
public interface CustomMapperScan {

}
