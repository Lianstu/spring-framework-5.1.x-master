package com.lianxiaoshuai.test_observer42.testObserver;

import java.util.Observable;
import java.util.Observer;

/**
 * 作为一个观察者、这个对应要封装在被观察者里面(被观察者变化时通知观察者)
 */
public class ObserverLister implements Observer{
	@Override
	public void update(Observable o, Object arg) {
		System.out.println("ObserverLister is update");
	}
}
