package com.lianxiaoshuai.test_observer42.testObserver;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import java.util.Observable;
import java.util.Observer;

/**
 *
 *
 */
public class Test {
	public static void main(String[] args) {
		/**
		 * 1. Observer 是个接口 观察者 implements Observer 重写update方法
		 *
		 * 2. ObservableObj extends Observable, Observable 是个类、使用时要setChanged、notifyObservers才能通知到
		 *
		 * 3. 被观察者 类里面是封装了 观察者, 被观察者变化时通知观察者(关注者要重写update)
		 *
		 * 4. 缺点： Observable 是个类、只能单继承、影响后续扩展，缺少事件类型的抽象
		 *
		 * 5. spring boot 中有很多的事件、观察者模式, 对不同事件做了很多抽象
		 *
		 * 	ApplicationEvent#ApplicationEvent(Object source)  封装了是那个源
		 *
		 * 	ApplicationListener#ApplicationListener<E extends ApplicationEvent>
		 *
		 *  applicationContext.publishEvent(new ApplicationEvent(source, receiver, content));
		 */
		Observer observerLister = new ObserverLister();
		ObservableObj observableObj = new ObservableObj();
		observableObj.addObserver(observerLister);
		observableObj.setChanged();

		observableObj.notifyObservers();

		ApplicationContext applicationContext;
		ApplicationEvent applicationEvent;
		ApplicationListener applicationListener;
	}
}
